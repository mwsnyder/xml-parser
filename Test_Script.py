import os
from xml.etree import ElementTree
import glob
import csv

# file_name = '8404232.xml'
# full_file = os.path.abspath(os.path.join('data', file_name))     #data is folder name

file = open('8404232.xml', 'r')

dom = ElementTree.parse(file)
awards = dom.findall('Award')

for a in awards:

        AwardID = a.find('AwardID').text
        Title = a.find('AwardTitle').text
        AwardAmount = a.find('AwardAmount').text
        AwardAmount = '${:\,.2f}'.format(int(AwardAmount))
        StartDate = a.find('AwardEffectiveDate').text
        EndDate = a.find('AwardExpirationDate').text
        # Department = a.find('Department').text
        Institution = a.find('Institution').find('Name').text

        print()
        print(f"AwardID:      {AwardID}")
        print(f"Title:        {Title}")
        print(f"Award Amount: {AwardAmount}")
        print(f"Start Date:   {StartDate}")
        print(f"End Date:     {EndDate}")
        # print(f"Organization: {Department}")
        print(f"Institution:  {Institution}")

        # print(' * {} [{}] ${} {} {} {} ' .format(
        #         AwardID, Title, AwardAmount, StartDate, EndDate, Organization
        # ))

for filename in glob.glob('*.xml'):
        print(filename)

# with open('temp.csv', "wb") as csv_file:
#         writer = csv.writer(csv_file, delimiter=',')
        # for line in data:
        #     writer.writerow(line)
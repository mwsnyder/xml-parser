# **This is the Title (H1)**

## **This is how you use the program (H2)**


**Important Questions:**

1. Will all of the XML files be identical, similar, or different?

2.

Documentation options:

1. Simple; How to use the program.

2. Extensive; Documentation on how and why the program works at a high level.

(Program will have inline documentation)


***

_Testing Markdown_

Here is how you use the code:

`command_line> python XML_parser.py required_parameter [optional_parameter]`

```
usage: app_name [options] required_input required_input2
  options:
    -a, --argument     Does something
    -b required     Does something with "required"
    -c, --command required     Something else
    -d [optlistitem1 optlistitem 2 ... ]     Something with list
```
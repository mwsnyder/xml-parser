from xml.etree import ElementTree
import glob

csv_file = open('temp.csv', 'w+')
error_log = open('error_log.txt', 'w+')


def fileToLine(filename):
    line = ''
    xml_file = open(filename, 'r')
    dom = ElementTree.parse(xml_file)

    for element in dom.findall('Award'):
        line += element.find('AwardID').text + ','
        line += element.find('AwardTitle').text + ','
        line += element.find('Institution').find('Name').text + ','
        AwardAmount = element.find('AwardAmount').text
        line += '${:.2f}'.format(int(AwardAmount)) + ','
        line += element.find('AwardEffectiveDate').text + ','
        line += element.find('AwardExpirationDate').text + ','

    print('Successfully read: %s', filename)

    return line + '\n'


def firstRow():
    line = ''

    line += 'Award ID,'
    line += 'Award Title,'
    line += 'Institution,'
    line += 'Award Amount,'
    line += 'Start Date,'
    line += 'End Date,'

    line += '\n'
    csv_file.write(line)


def main():

    firstRow()

    for filename in glob.glob('*.xml'):
        try:
            line = fileToLine(filename)
            csv_file.write(line)
        except Exception:
            print('An error occurred when processing: %s', filename)
            error_log.write('An error occurred when processing: ' + filename)


main()
